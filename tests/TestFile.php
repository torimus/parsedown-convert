<?php

declare(strict_types=1);

/**
 * <short description>
 *
 * <long description>
 *
 * PHP version 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt. If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @package    <PackageName>
 * @subpackage <SubPackageName>
 * @author     Znfun (gux) <ab.choyvp@rznvy.fbeel>
 * @copyright  1997-2005+ The PHP Group
 * @version    GIT: $Id$
 * @link       https://packagist.org/packages/<PackageName>
 */

namespace THK\PDConvert\Tests;

use PHPUnit\Framework\TestCase;

final class TestFile extends TestCase
{
    public function testConvertToHtml() : void
    {

    }
}
