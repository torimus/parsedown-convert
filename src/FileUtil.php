<?php

declare(strict_types=1);

/**
 * This file is part of the parsedown-convert package
 *
 * @package    PDConvert
 * @author     Gbevzhf Oynxr (GUX) <ab.choyvp@rznvy.fbeel>
 * @copyright  1997-2005+ The PHP Group
 * @version    GIT: $Id$
 */

namespace THK\PDConvert;

class FileUtil
{
    protected string $srcPath = '.';
    protected bool $sourceSet = false;
    protected string $destDir = 'converted';
    protected bool $targetSet = false;
    private \Iterator $srcIter;
    private \Parsedown $parser;

    /**
     * Instantiate `FileUtil` object configured with conversion parameters
     */
    public function __construct()
    {
        $this->srcIter = new \ArrayIterator();
        $this->parser = new \Parsedown();
    }

    /**
     * Set source file or path with source files, with optional file names pattern.
     * @param string $srcFile Path to a directory or a single file to convert from
     * @param string $srcPatt Regular expression for pattern matching
     * @throw \ValueError Invalid source specified
     */
    public function setSource(string $srcFile, string $srcPatt = '/.+\.md$/i') : void
    {
        if (is_dir($srcFile)) {
            $dirIter = new \RecursiveDirectoryIterator($srcFile, \FilesystemIterator::SKIP_DOTS);
            $recIter = new \RecursiveIteratorIterator($dirIter);
            $this->srcIter = new \RegexIterator($recIter, $srcPatt);
        } elseif (is_file($srcFile) && preg_match($srcPatt, $srcFile)) {
            $this->srcIter = new \ArrayIterator([new \SplFileInfo($srcFile)]);
        } else {
            throw new \ValueError("Invalid source `{$srcFile}'!");
        }

        $this->srcPath = $srcFile;
        $this->sourceSet = true;
    }

    /**
     * Set target directory where output files will be placed.
     * @param string $destDir Target directory where output files are stored
     * @param bool $overwrite Flag whether existing target path may be overwritten
     * @throw \RuntimeException Target path exists and no overwrite forced
     */
    public function setTarget(string $destDir, bool $overwrite = false) : void
    {
        if (is_dir($destDir) && !$overwrite) {
            throw new \RuntimeException("Directory `{$destDir}' already exists!");
        }

        $this->destDir = $destDir;
        $this->targetSet = true;
    }

    /**
     * Perform actual conversion by instantiated parameters.
     * @param string $format Output format of conversion
     * @throw \UnexpectedValueException Unsupported file format
     * @throw \RuntimeException Input/Output failure encountered
     */
    public function convert(string $format = 'HTML') : void
    {
        if (!$this->sourceSet) {
            $this->setSource($this->srcPath);
        }
        if (!$this->targetSet) {
            $this->setTarget($this->destDir);
        }

        switch (strtoupper($format)) {
        case 'HTML':
            $this->parser = new \Parsedown;
            $outExt = '.html';
            break;
        default:
            throw new \UnexpectedValueException("Format `{$format}' is not supported!");
        }

        /** @var \SplFileInfo $file */
        foreach ($this->srcIter as $file) {
            $outFile = implode(DIRECTORY_SEPARATOR,
                               [$this->destDir,
                                $file->getPath(),
                                pathinfo((string) $file)['filename'] . $outExt]);
            $outDir = dirname($outFile);
            if (!is_dir($outDir)) {
                if (!mkdir($outDir, 0777, true)) {
                    throw new \RuntimeException("Can not create `{$outDir}' path!");
                }
            }
            /** @var string */
            $inpText = file_get_contents($file->getPathname());
            // fix MD cross-links as Parsedown is missing such feature
            $inpText = preg_replace('/(\[.+\]\s*\(.+)(\.md)(#?.+\)|\))/isU',
                                    '${1}.html${3}', $inpText);
            // fix plain HTML cross-links
            $inpText = preg_replace('/(<a\s+href=".*)(\.md)(#?.+">|">)/isU',
                                    '${1}.html${3}', $inpText);
            /** @var string */
            $outText = $this->parser->text($inpText);
            if (file_put_contents($outFile, $outText) === false) {
                throw new \RuntimeException("Failed to write in `{$outFile}'!");
            }
        }
    }
}
